package com.mobstac.onboarding;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import com.mobstac.beaconstac.Beaconstac;
import com.mobstac.beaconstac.core.MSException;
import com.mobstac.beaconstac.interfaces.MSErrorListener;
import com.mobstac.beaconstac.interfaces.BeaconScannerCallbacks;
import com.mobstac.beaconstac.interfaces.MSSyncListener;
import com.mobstac.beaconstac.models.MBeacon;
import com.mobstac.beaconstac.models.MRule;

public class MainActivity extends AppCompatActivity {

    TextView resultView = null;
    EditText lastName = null;
    EditText pnrNum = null;
    Button submit = null;
    ImageView qr_img = null;
    int beaconid = 0;
    MainActivity main = null;
    Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultView = findViewById(R.id.textView4);
        lastName = findViewById(R.id.editText);
        pnrNum = findViewById(R.id.editText2);
        submit = findViewById(R.id.button2);
        qr_img = findViewById(R.id.imageView2);
        main = new MainActivity();

        // Initialize scanning of Beacons
        initializeAction();

        ToggleButton toggle = findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    startToggle();
                } else {
                    // The toggle is disabled
                    stopToggle();
                }
            }
        });

        // Set Name and Email
        Beaconstac.getInstance().setUserName("Varun","Singh");
        Beaconstac.getInstance().setUserEmail("varun@mobstac.com");

        Beaconstac.getInstance().setBeaconScannerCallbacks(new BeaconScannerCallbacks() {

            @Override
            public void onCampedBeacon(MBeacon beacon) {
                System.out.println("On Camped Beacon Triggered");
                // String beaconsno = beacon.getSerialNumber();
                beaconid = beacon.getId();
                // int beaconpid = beacon.getPlaceId();

                /**
                * resultView.append("Beacon Serial : " + beaconsno + "\n");
                * resultView.append("Beacon ID : " + Integer.toString(beaconid) + "\n");
                * resultView.append("Beacon Place ID : " + Integer.toString(beaconpid) + "\n");
                */
            }

            @Override
            public void onScannedBeacons(ArrayList<MBeacon> rangedBeacons) {
            }

            @Override
            public void onExitedBeacon(MBeacon beacon) {
            }

            @Override
            public void onRuleTriggered(MRule rule) {
            }


        });

        submit.setOnClickListener(
                new View.OnClickListener()  {
                    public void onClick(View view)  {
                        //Log.v("EditText", mEdit.getText().toString());
                        resultView.append("Last Name : " + lastName.getText().toString() + "\n");
                        resultView.append("PNR Num: " + pnrNum.getText().toString() + "\n");
                        DBUtil db = new DBUtil();
                        db.dbOperations();
                    }

                });

        /*
        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    resultView.append(lastName.getText().toString());
                    System.out.println(lastName.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });

        pnrNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    resultView.append(pnrNum.getText().toString());
                    System.out.println(pnrNum.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });
        */
    }

    public class DBUtil {
        public void dbOperations()
        {
            DatabaseConnect db = new DatabaseConnect();
            db.generatePass(lastName.getText().toString(),pnrNum.getText().toString(),beaconid,ctx);
            resultView.append("Beacon Id: " + beaconid + "\n");
            resultView.append("From:" + db.getFrom() + "\n");
            resultView.append("To:" + db.getTo() + "\n");
            resultView.append("Gate Number:" + db.getGate() + "\n");
            resultView.append("Seat Number:" + db.getSeat() + "\n");
            resultView.append("Flight Number:" + db.getFlight() + "\n");
            qr_img.setImageBitmap(db.getBitMap());
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        Beaconstac.getInstance().stopScanningBeacons(null);
        initializeAction();
    }

    private void initializeAction() {
        try {
            Beaconstac.initialize(getApplicationContext(), "e62435a78e67ec98bba3b879ba00448650032557", new MSSyncListener() {

                @Override
                public void onSuccess() {
                    Log.d("Beaconstac", "Initialization successful");
                    Beaconstac.getInstance().startScanningBeacons(new MSErrorListener() {
                        @Override
                        public void onError(MSException msException) {

                        }
                    });
                }

                @Override
                public void onFailure(MSException e) {
                    Log.d("Beaconstac", "Initialization failed");
                }

            });
        }
        catch (MSException | SecurityException e) {
            e.printStackTrace();
        }
    }

    private void stopToggle(){
        try {
            Beaconstac.getInstance().stopScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Stopped Scanning");
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void startToggle(){
        try {
            Beaconstac.getInstance().startScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Started Scanning");
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}

